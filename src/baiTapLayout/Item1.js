import React, { Component } from 'react'

export default class Item1 extends Component {
  render() {
    return (
        <div
        style={{ maxWidth: "47%", marginTop: "-50px" }}
        className="bg-light mb-5 p-5"
      >
        <div
          style={{ width: "4rem", height: "4rem", marginTop: "-75px" }}
          className="d-flex rounded-lg justify-content-center align-items-center button-outside bg-secondary bg-gradient text-white mx-auto mb-3"
        >
          <i style={{ fontSize: "2rem" }} class="fa fa-cloud-download-alt"></i>
        </div>
        <div className="layout container">
          <h3>Free Download</h3>
          <p>
            As always, Start Bootstrap has a powerful collectin of free
            templates.
          </p>
        </div>
      </div>
    )
  }
}

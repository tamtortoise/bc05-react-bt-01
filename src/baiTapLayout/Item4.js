import React, { Component } from 'react'

export default class Item4 extends Component {
  render() {
    return (
        <div
        style={{ maxWidth: "47%", marginTop: "-50px" }}
        className="bg-light mb-5 p-5"
      >
        <div
          style={{ width: "4rem", height: "4rem", marginTop: "-75px" }}
          className="d-flex rounded-lg justify-content-center align-items-center button-outside bg-dark bg-gradient text-white mx-auto mb-3"
        >
          <i style={{ fontSize: "2rem" }} class="fa fa-code"></i>
        </div>
        <div className="layout container">
          <h3>Simple clean code</h3>
          <p>
            We keep our dependencies up to date and squash bugs as they come!
          </p>
        </div>
      </div>
    )
  }
}

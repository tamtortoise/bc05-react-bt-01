import React, { Component } from 'react'

export default class Item2 extends Component {
  render() {
    return (
        <div
        style={{ maxWidth: "47%", marginTop: "-50px" }}
        className="bg-light mb-5 p-5"
      >
        <div
          style={{ width: "4rem", height: "4rem", marginTop: "-75px" }}
          className="d-flex rounded-lg justify-content-center align-items-center button-outside bg-warning bg-gradient text-white mx-auto mb-3"
        >
          <i style={{ fontSize: "2rem" }} class="fa fa-id-card"></i>
        </div>
        <div className="layout container">
          <h3>Jumbotron hero header</h3>
          <p>The heroic part of this template is the jumbotron hero header!</p>
        </div>
      </div>
    )
  }
}

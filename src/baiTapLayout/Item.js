import React, { Component } from 'react'

export default class Item extends Component {
  render() {
    return (
        <div
        style={{ width: "47%", marginTop: "-50px" }}
        className="bg-light mb-5 p-5"
      >
        <div
          style={{ width: "4rem", height: "4rem", marginTop: "-75px" }}
          className="d-flex rounded-lg justify-content-center align-items-center button-outside bg-danger bg-gradient text-white mx-auto mb-3"
        >
          <i style={{ fontSize: "2rem" }} class="fa fa-layer-group"></i>
        </div>
        <div className="layout container">
          <h3>Fresh Package</h3>
          <p>
           Full of supprise
          </p>
        </div>
      </div>
    )
  }
}

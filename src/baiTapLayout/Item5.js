import React, { Component } from 'react'

export default class Item5 extends Component {
  render() {
    return (
        <div
        style={{ maxWidth: "47%", marginTop: "-50px" }}
        className="bg-light mb-5 p-5"
      >
        <div
          style={{ width: "4rem", height: "4rem", marginTop: "-75px" }}
          className="d-flex rounded-lg justify-content-center align-items-center button-outside bg-primary bg-gradient text-white mx-auto mb-3"
        >
          <i style={{ fontSize: "2rem" }} class="fa fa-check-double"></i>
        </div>
        <div className="layout container">
          <h3>A name you trust</h3>
          <p>
            Start Bootstrap has been the leader in free Bootstrap templates
            since 2013!
          </p>
        </div>
      </div>
    )
  }
}

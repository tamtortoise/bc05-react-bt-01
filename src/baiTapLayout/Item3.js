import React, { Component } from 'react'

export default class Item3 extends Component {
  render() {
    return (
        <div
        style={{ maxWidth: "47%", marginTop: "-50px" }}
        className="bg-light mb-5 p-5"
      >
        <div
          style={{ width: "4rem", height: "4rem", marginTop: "-75px" }}
          className="d-flex rounded-lg justify-content-center align-items-center button-outside bg-info bg-gradient text-white mx-auto mb-3"
        >
          <i style={{ fontSize: "2rem" }} class="fab fa-bootstrap"></i>
        </div>
        <div className="layout container">
          <h3>Feature boxes</h3>
          <p>We've created some custom feature boxes using Bootstrap icons!</p>
        </div>
      </div>
    )
  }
}

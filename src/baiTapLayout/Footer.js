import React, { Component } from 'react'

export default class Footer extends Component {
  render() {
    return (
      <div className="text-white bg-dark py-5 justify-content-center align-items-center">
        <p>Copyright © Your Website 2022</p>
      </div>
    )
  }
}
